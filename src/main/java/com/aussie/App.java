package com.aussie;

import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by andrey.kondratyev on 07.09.2016.
 */
public class App {
    public void appendToZip(String dataDir, String outFile, String appendFile) {
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");

        Path path = Paths.get(dataDir + File.separator + outFile);
        URI uri = URI.create("jar:" + path.toUri());

        try {
            try (FileSystem fs = FileSystems.newFileSystem(uri, env)) {
                Path nf = fs.getPath("ppt/slides/" + appendFile);

                try (Writer writer = Files.newBufferedWriter(nf, StandardCharsets.UTF_8, StandardOpenOption.CREATE)) {
                    int len;
                    char[] buffer = new char[1024];

                    Path source = Paths.get(dataDir + File.separator + appendFile);
                    try (Reader reader = Files.newBufferedReader(source, StandardCharsets.UTF_8)) {

                        while ((len = reader.read(buffer)) > 0) {
                            writer.write(buffer, 0, len);
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e);
        }
    }


    public static void main(String[] args) {
        App app = new App();
        app.appendToZip("data", "zip_without_xml.zip", "slide1.xml");
    }
}